## 交流群

可以加QQ群交流讨论：<a target="_blank" href="https://qm.qq.com/cgi-bin/qm/qr?k=VCHLj39ZJjRh-vBIAOm65UVhCTS8pi3P&jump_from=webapi"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="实验楼项目学习交流群" title="实验楼项目学习交流群"></a>

## 部署演示视频

[部署视频](https://mp.weixin.qq.com/s/6riSAJDw6-f80GOE1eWb8w)

## 项目讲解视频

[讲解视频](https://mp.weixin.qq.com/s/jJMv9c9w0vbBPh6MXB7xsA)

## 演示视频

[基于LayuiIM框架的聊天项目](https://www.ixigua.com/i6802197033609331204/)

## 1.介绍

- 技术架构：Springboot+websocket+MySQL+LayuiIM
- 注意事项：**此项目仅供交流学习，不可用于商业用途(如果出现问题本人概不负责),更不可出售本项目源代码。**
- **本项目的LayuIM代码未上传，LayuiIM代码是Layui收费的项目，如有需要可以去购买或通过其他途径获取。** 
- 本项目所有前端主要JS和后台主要接口都加有注释，所以很容易看得懂
- 另外本项目如有不足之处，尽请谅解，也欢迎指出。

## 2.项目截图

### 1.聊天窗口界面

![在这里插入图片描述](https://images.gitee.com/uploads/images/2020/0312/092922_304e9c88_3026905.png)

### 2.好友列表界面

![在这里插入图片描述](https://images.gitee.com/uploads/images/2020/0312/092922_b572f430_3026905.png)

### 3.聊天记录界面

![在这里插入图片描述](https://images.gitee.com/uploads/images/2020/0312/092922_2aa5c69f_3026905.png)

### 4.收到消息提醒样式

![在这里插入图片描述](https://images.gitee.com/uploads/images/2020/0312/092922_f8793386_3026905.png)

### 5.群组聊天

![在这里插入图片描述](https://images.gitee.com/uploads/images/2020/0603/134338_50bd3360_3026905.png)

### 6.整体样式

![在这里插入图片描述](https://images.gitee.com/uploads/images/2020/0312/092922_92945edd_3026905.png)

## 3.安装教程

1.首先拉取拉取代码 [仓库地址](https://gitee.com/xzlmk/Layui-IM)

```bash
git pull https://gitee.com/xzlmk/Layui-IM.git
```

2.将项目中的数据库文件夹下的layuiim.sql导入到MySQL数据库，数据库名称为layuiim。

3.将数据库文件夹下面的layuichat文件夹放在D盘chat目录下面(这是数据库中的聊天数据文件)

4.使用idea打开项目，运行项目

5.从数据库的userinfo表中
选择一个userid（最好是1571476959767947449），
然后在`一个`浏览器中访问：[http://localhost:8080/layuiim/1571476959767947449](http://localhost:8080/layuiim/1571476959767947449)
然后再选择一个userid，最好是（1571476959767947441），
然后在`另一个`浏览器中访问：[http://localhost:8080/layuiim/1571476959767947441](http://localhost:8080/layuiim/1571476959767947441)
选择这两个`userid`的原因是数据库初始数据让这两人互为好友了。

## 4.项目接口

1、/chat/upimg

>这个接口就是聊天图片上传的的接口

2、/chat/upfile

>这个接口就是聊天文件上传的的接口

3、/chat/upsigin

>这个接口是用来更新用户的签名接口

4、/layuiim/{userid}

>跳转到聊天界面，这个传过来的userid充当用户登录的id

5、/tochatlog

>跳转到聊天记录界面

6、/chatlog/{uid}

>这个接口是查询用户之间的聊天记录的，uid是好友的id

7、/initim

>这个接口是用来初始化聊天界面的，需要查询用户信息、用户好友列表、用户群组。

## 5.项目功能

- 两人之间私聊、多人之间的群聊
- 可以查看两人的聊天记录、群组的聊天记录
- 可以发送图片、文件，对于文件点击直接下载
- 可以方式网络音频、视频，点击直接播放
- 新消息来后可以音频提醒
- 更新个性签名
- 用户默认状态为离线，当用户上线后更新状态为在线，当用户关闭客户端更新状态为离线
- 更多功能敬请期待.....

## 6.更新日志

- 2020-03-27
    
    >增加群聊功能
- 2020-03-09
    
    >Layui-IM聊天上线
