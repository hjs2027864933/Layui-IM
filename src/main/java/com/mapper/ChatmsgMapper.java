package com.mapper;

import com.bean.Chatmsg;
import com.bean.Mine;

import java.util.List;

public interface ChatmsgMapper {
    //插入发送的消息记录
    void insertChatmsg(Chatmsg chatmsg);
    //查询聊天记录
    List<Mine> LookChatMsg(Chatmsg chatMsg);
}