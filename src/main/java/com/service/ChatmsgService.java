package com.service;

import com.bean.Chatmsg;
import com.bean.Mine;
import com.mapper.ChatmsgMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChatmsgService {
    @Autowired
    ChatmsgMapper chatmsgMapper;
    @Async
    public void insertChatmsg(Chatmsg chatmsg){
        chatmsgMapper.insertChatmsg(chatmsg);
    }
    public List<Mine> LookChatMsg(Chatmsg chatMsg){
        return chatmsgMapper.LookChatMsg(chatMsg);
    }
}
